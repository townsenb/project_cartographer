package me.bentownsend.cartographer

import android.graphics.drawable.VectorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.content.res.AppCompatResources
import com.devs.vectorchildfinder.VectorChildFinder

class MainActivity : AppCompatActivity() {

    lateinit var playerCardViews: MutableList<ImageView>
    lateinit var opponentCardViews: MutableList<ImageView>


    fun initCardViews(){
        playerCardViews = mutableListOf()
        opponentCardViews = mutableListOf()


        playerCardViews.add(findViewById(R.id.playerCard1))
        playerCardViews.add(findViewById(R.id.playerCard2))
        playerCardViews.add(findViewById(R.id.playerCard3))
        playerCardViews.add(findViewById(R.id.playerCard4))
        playerCardViews.add(findViewById(R.id.playerCard5))
        playerCardViews.add(findViewById(R.id.playerCard6))
        playerCardViews.add(findViewById(R.id.playerCard7))
        playerCardViews.add(findViewById(R.id.playerCard8))
        playerCardViews.add(findViewById(R.id.playerCard9))


        opponentCardViews.add(findViewById(R.id.opponentCard1))
        opponentCardViews.add(findViewById(R.id.opponentCard2))
        opponentCardViews.add(findViewById(R.id.opponentCard3))
        opponentCardViews.add(findViewById(R.id.opponentCard4))
        opponentCardViews.add(findViewById(R.id.opponentCard5))
        opponentCardViews.add(findViewById(R.id.opponentCard6))
        opponentCardViews.add(findViewById(R.id.opponentCard7))
        opponentCardViews.add(findViewById(R.id.opponentCard8))
        opponentCardViews.add(findViewById(R.id.opponentCard9))

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        initCardViews()

        var playerCard1 = Card("Ebleb",CardType.ROCK,CardStatus.GOLD,"",0xCACACA,null)
        val vectFind = VectorChildFinder(this.baseContext,R.drawable.card_template,playerCardViews[0])
        vectFind.findPathByName("status_border").strokeColor = R.color.diamond
        playerCard1.vector = AppCompatResources.getDrawable(this.baseContext,R.drawable.card_template) as VectorDrawable
        playerCardViews[0].setImageDrawable(playerCard1.vector)
    }
}