package me.bentownsend.cartographer

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.VectorDrawable
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import com.devs.vectorchildfinder.VectorChildFinder

enum class CardStatus{
    BRONZE, SILVER, GOLD, DIAMOND
}

enum class CardType{
    ROCK, PAPER, SCISSORS
}


class Card (val name: String, val type: CardType, val status: CardStatus, val description: String, val bgColor: Int, val picture: Bitmap?){

    var vector: VectorDrawable? = null

    val borderColor: Int = when(status){
        CardStatus.BRONZE -> R.color.bronze
        CardStatus.SILVER -> R.color.silver
        CardStatus.GOLD -> R.color.gold
        CardStatus.DIAMOND -> R.color.diamond
    }

    val typeColor: Int = when(type){
        CardType.ROCK -> R.color.rock
        CardType.PAPER -> R.color.paper
        CardType.SCISSORS -> R.color.scissors
    }


    //fun cardDataToVectorDrawable(): VectorDrawable{
        //val vectFind = VectorChildFinder(context,R.drawable.card_template,imgView)

        //vectFind.findPathByName("inner_color").setFillColor(borderColor)

        //println(Integer.toHexString(vectFind.findPathByName("inner_color").fillColor).toString() + " POOP")



        //vectFind.findPathByName("type_circle").fillColor = typeColor

        //vectFind.findPathByName("inner_color").fillColor = bgColor

        //return AppCompatResources.getDrawable(context,R.drawable.card_template) as VectorDrawable
    //}
}